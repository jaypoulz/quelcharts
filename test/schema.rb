create_table :visualizations, :force => true do |t|
  t.json :data
  t.json :configs
end
