module Quelcharts
  class Engine < ::Rails::Engine
    initializer 'quelcharts.load_static_assets' do |app|
      app.middleware.use ::ActionDispatch::Static, "#{root}/vendor"
    end
  end
end
