module Quelcharts
  module Visualization
    def create(vis_params)
      ##
      #@visualization = Visualization.new(vis_params)
      #
      #respond_to do |format|
      #  if @visualization.save
      #    flash[:notice] = 'Visualization was successfully created.'
      #    format.html { redirect_to @visualization }
      #    format.json { render json: @visualization.to_hash(false), status: :created }
      #  else
      #    format.json { render json: @visualization.errors, status: :unprocessable_entity }
      #  end
      #end
      ###
    end

    def show
       @visualization = Visualization.find(params[:id])
    end

    def draw_visualiztion()
      draw_visualization(data, configs)
    end

    def update(configs)
    end

    protected
    def draw_visualization(data, configs)
      draw_axes(data, configs)
      draw_controls(data, configs)
      plot_data(data, configs)
    end

    def draw_axes(data, configs)
    end

    def draw_controls(data, configs)
    end

    def plot_data(data, configs)
    end
  end
end
