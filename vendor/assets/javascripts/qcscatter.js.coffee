##
# qcscatter.js.coffee
#
# This class defines Scatter specific code.
# Overrides quelchart.js.coffee defaults.
$ ->
  class window.QCScatter extends QuelChart

    constructor: (id = 0) ->
      super(id)
      @points = []

    drawChart: (data, configs, resetId = false) ->
      super(data, configs, resetId)
      @drawPoints()

    ensureDefaults: ->
      @configs.qccblacklist = []
      super()

    ### Draw Points ###
    drawPoints: ->
      @points = []
      x = @configs.independent
      @setOrigin(@ctx.m, @bbox.bl.x, @bbox.bl.y)

      for row, idx in @data
        for y in @configs.dependent
          xval = row[x]
          yval = row[y]
          xpos = (xval - @xmin) * @bbox.width / @xrange
          ypos = (yval - @ymin) * @bbox.height / @yrange

          color = @seriesColor(idx)
          @GLYPHS.circle(xpos, ypos, @ctx.m, color)
          @points.push { id: idx, x: xpos, y: ypos }

      @restore(@ctx.m)

    ### Label Axes ###
    labelXAxis: ->
      @setOrigin(@ctx.x, @bbox.bl.x, @cheight)
      col = @configs.gridColumns
      interval = @bbox.width / col

      @ctx.x.font = 'italic 8pt sans-serif'
      @ctx.x.scale(1, -1)
      @ctx.x.fillStyle = '#888'
      @ctx.x.textAlign = 'center'

      for n in [0..col]
        x = n * interval
        y = -@AXIS_MARGIN_X + @LABEL_MARGIN
        label = Math.roundTo(n * @xrange / col + @xmin, 3).toString()
        @ctx.x.fillText(label, x, y)

      @restore(@ctx.x)

    onselect: (o, w, h) ->
      super(o, w, h)

      canvas = $('.qccanvas-wrapper')[@id].getBoundingClientRect()
      o.x = o.x - (canvas.x + @bbox.bl.x)
      o.y = canvas.top + @bbox.bl.y - o.y

      points = @points.filter (p) ->
        (p.x > o.x) and (p.x < o.x + w) and (p.y < o.y) and (p.y > o.y - h)

      for point in points
        @selected.push point.id

      @selected

    end: ->
      super()
