##
# qccarousel.js.coffee
#
# This class defines Carousel specific code.
$ ->
  class window.QCCarousel

    constructor: (vises) ->
      # assign vises unique ids
      @vises = vises
      @qcc = null

    draw: (data, configs) ->
      @data = data
      @configs = configs

      @qcc ?= new qcconfigs.QCCarousel()

      for vis, id in @vises when id < 4
        vis.initWithId(id)
        vis.drawChart(data, configs, false)

    end: ->
      if @qcc?
        @qcc.end()
        delete @qcc

    rotateLeft: ->
      leftMost = @vises.shift()
      @vises.push(leftMost)
      @draw(@data, @configs)

    rotateRight: ->
      rightMost = @vises.pop()
      @vises.unshift(rightMost)
      @draw(@data, @configs)
