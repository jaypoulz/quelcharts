##
# dragSelection.js.coffee
#
# This module takes care of mouse selection events.
$ ->
  window.setSelectionListener = (id, mufn, mmfn = ->) ->
    isSelection = false
    selBox = $('#select-box')
    selBoxObj = selBox[0]

    getCursor = (e) ->
      m = {}
      m.x = e.pageX
      m.y = e.pageY
      m

    getQuadrant = (o, m) ->
      if m.x >= o.x and m.y >= o.y
        return 4
      else if m.x >= o.x and m.y < o.y
        return 1
      else if m.x < o.x and m.y >= o.y
        return 3
      else return 2

    $(id).mousedown (e) ->
      isSelection = true
      mouse = getCursor(e)

      selBoxObj.origin = mouse
      selBox.css({
        'left': mouse.x + 'px',
        'top': mouse.y + 'px',
        'width': '0',
        'height': '0',
        'transform': ''
      })
      selBox.show()

    $(document).mouseup (e) ->
      if isSelection
        selBox.hide()
        isSelection = false

        o = selBoxObj.origin
        m = getCursor(e)
        q = getQuadrant(o, m)
        w = Math.floor(selBox.width())
        h = Math.floor(selBox.height())

        switch q
          when 2
            o = m
          when 3
            o.x = m.x
          when 1
            o.y = m.y

        # Do the thing
        mufn(o, w, h)

    $(document).mousemove (e) ->
      if isSelection
        o = selBoxObj.origin
        ro = { x: o.x, y: o.y }
        m = getCursor(e)
        q = getQuadrant(o, m)
        w = Math.abs(m.x - o.x) + 'px'
        h = Math.abs(m.y - o.y) + 'px'

        rotate = 'rotate(0deg)'

        if q is 3 or q is 1
          t = w
          w = h
          h = t

        switch q
          when 2
            rotate = 'rotate(180deg)'
          when 3
            rotate = 'rotate(90deg)'
            ro.x = m.x
          when 1
            rotate = 'rotate(270deg)'
            ro.y = m.y

        selBox.css({
          'width': w,
          'height': h,
          'transform': rotate;
        })

        # Do the thing
        mmfn(ro, w, h)
