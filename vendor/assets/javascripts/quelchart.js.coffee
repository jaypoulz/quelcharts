##
# quelchart.js.coffee
#
# This class is the base charting class which is also the external API.
# Scatter will override very few of these defaults.
$ ->
  class window.QuelChart

    DEFAULT:
      GRID_TICK_MAJ:  12
      GRID_TICK_MIN:   6
      GRID_ROWS:      12
      GRID_COLUMNS:   12
      SERIES_COLOR:             'blue'
      SERIES_HIGHLIGHT:         'deepskyblue'
      SERIES_CONTAINSHIGHLIGHT: 'turquoise'

    constructor: (id = 0) ->
      @ctx = {}
      @canvas = {}
      @selected = []
      @data = []
      @configs = {}

      @initWithId(id)

      #$(document).resize ->
      #  @redrawChart()

      # These are the point glyphs
      @GLYPHS = {}
      @GLYPHS.circle = (x, y, ctx, color) =>
        ctx.beginPath()
        ctx.arc(x, y, 5, 0, 2 * Math.PI)
        ctx.fillStyle = color
        ctx.closePath()
        ctx.fill()

    initWithId: (id) ->
      @id = id

      @canvas.m = $('.qc-main')[id]
      @canvas.x = $('.qc-xaxis')[id]
      @canvas.y = $('.qc-yaxis')[id]
      @canvas.t = $('.qc-title')[id]
      @canvas.g = $('.qc-grid')[id]

      @ctx.m = @canvas.m.getContext('2d')
      @ctx.x = @canvas.x.getContext('2d')
      @ctx.y = @canvas.y.getContext('2d')
      @ctx.t = @canvas.t.getContext('2d')
      @ctx.g = @canvas.g.getContext('2d')

    refresh: ->
      @cwidth = $($('.qccanvas-wrapper')[@id]).width()
      @cheight = $($('.qccanvas-wrapper')[@id]).height()

      @canvas.m.width = @cwidth
      @canvas.x.width = @cwidth
      @canvas.y.width = @cwidth
      @canvas.t.width = @cwidth
      @canvas.g.width = @cwidth

      @canvas.m.height = @cheight
      @canvas.x.height = @cheight
      @canvas.y.height = @cheight
      @canvas.t.height = @cheight
      @canvas.g.height = @cheight

      @AXIS_MARGIN_Y = .10  * @cwidth
      @AXIS_MARGIN_X = .15  * @cheight
      @TITLE_MARGIN  = .15  * @cheight
      @LABEL_MARGIN  = .075 * @cheight

      ##
      # This is the bounding box for the vis drawing
      #
      # tr = top right
      # br = bottom right
      # tl = top left
      # bl = bottom left
      # cp = center point
      @bbox = {
        tr:
          x: Math.floor(@cwidth - @AXIS_MARGIN_Y)
          y: Math.floor(@TITLE_MARGIN)
        br:
          x: Math.floor(@cwidth - @AXIS_MARGIN_Y)
          y: Math.floor(@cheight - @AXIS_MARGIN_X)
        tl:
          x: Math.floor(@AXIS_MARGIN_Y)
          y: Math.floor(@TITLE_MARGIN)
        bl:
          x: Math.floor(@AXIS_MARGIN_Y)
          y: Math.floor(@cheight - @AXIS_MARGIN_X)
        cp:
          x: Math.floor((@cwidth - 2 * @AXIS_MARGIN_Y) / 2)
          y: Math.floor((@cheight - (@TITLE_MARGIN + @AXIS_MARGIN_X)) / 2)
        width: Math.floor(@cwidth - 2 * @AXIS_MARGIN_Y)
        height: Math.floor(@cheight - (@AXIS_MARGIN_X + @TITLE_MARGIN))
      }

    setOrigin: (ctx, x, y) ->
      ctx.save()
      ctx.translate(x, y)
      ctx.scale(1, -1)

    getData: ->
      return @data

    getConfigs: ->
      return @configs

    drawChart: (data, configs, resetId = false) ->
      if resetId then @initWithId(0)
      @data = data
      @configs = configs
      if configs.selected? then @selected = configs.selected

      @clear()
      @refresh()

      @ensureDefaults()
      @setDataBoundaries()
      @manageQCConfigs()

      @drawTitle()
      @drawAxes()
      @drawGrid()
      @labelAxes()

    redrawChart: (setSelection, resetId) ->
      if setSelection
        @configs.selected = @selected
      @drawChart(@data, @configs, resetId)

    # Redraw this chart on window resize
    #$(window).on 'resize', =>
      #@redrawChart(true)
      #clearTimeout(@resize)
      #@resize = setTimeout(@redrawChart(data, configs), 500)

    ### Ensures configurations are complete for drawing ###
    ensureDefaults: ->
      @configs.gridRows ?= @DEFAULT.GRID_ROWS
      @configs.gridColumns ?= @DEFAULT.GRID_COLUMNS
      @configs.qccmodules ?= ['QCAxisControls']
      @configs.qccblacklist ?= []
      @configs.fields ?= [0..@configs.headers.length - 1]
      @configs.numfields ?= @configs.fields
      @configs.textfields ?= []
      @configs.qcconfigs ?= []

    ### Sets the min, max, and range values ###
    setDataBoundaries: ->
      xs = for row in @data
        row[@configs.independent]

      ys_rowed =
        for y in @configs.dependent
          for row in @data
            row[y]

      ys = []
      ys = ys.concat.apply(ys, ys_rowed)

      @xcount = xs.length
      @ycount = ys.length
      @xmax = xs.reduce (a, b) -> Math.max(a, b)
      @xmin = xs.reduce (a, b) -> Math.min(a, b)
      @ymax = ys.reduce (a, b) -> Math.max(a, b)
      @ymin = ys.reduce (a, b) -> Math.min(a, b)
      @xrange = @xmax - @xmin
      @yrange = @ymax - @ymin

    ### Drawing Title ###
    drawTitle: ->
      @setOrigin(@ctx.t, @bbox.tl.x, @bbox.tl.y)

      @ctx.t.font = '16pt sans-serif'
      @ctx.t.fillStyle = '#555'
      @ctx.t.textAlign = "center"
      x = Math.floor(@bbox.width / 2)
      y = Math.floor(@TITLE_MARGIN / 2)
      @ctx.t.scale(1, -1)
      @ctx.t.fillText(@configs.title, x, -y)

      @restore(@ctx.t)

    ### Drawing Axes ###
    drawAxes: ->
      @drawYAxis()
      @drawXAxis()

    drawXAxis: ->
      @setOrigin(@ctx.x, @bbox.bl.x, @cheight)
      @ctx.x.beginPath()
      @ctx.x.lineWidth = 5
      @ctx.x.strokeStyle = '#888'
      @ctx.x.moveTo(0, @AXIS_MARGIN_X)
      @ctx.x.lineTo(@bbox.width, @AXIS_MARGIN_X)
      @ctx.x.closePath()
      @ctx.x.stroke()

      @restore(@ctx.x)

    drawYAxis: ->
      @setOrigin(@ctx.y, 0, @bbox.bl.y)
      @ctx.y.beginPath()
      @ctx.y.lineWidth = 5
      @ctx.y.strokeStyle = '#888'
      @ctx.y.moveTo(@AXIS_MARGIN_Y, 0)
      @ctx.y.lineTo(@AXIS_MARGIN_Y, @bbox.height)
      @ctx.y.closePath()
      @ctx.y.stroke()

      @restore(@ctx.y)

    ### Draw Axes Grid ###
    drawGrid: ->
      @drawXGrid()
      @drawYGrid()

    drawXGrid: ->
      @setOrigin(@ctx.g, @bbox.bl.x, @bbox.bl.y)
      col = @configs.gridColumns
      interval = @bbox.width / col
      @ctx.g.strokeStyle = '#AAA'
      @ctx.g.fillStyle = '#AAA'
      @ctx.g.lineWidth = 1

      for n in [1..col]
        @ctx.g.beginPath()
        x = Math.floor(n * interval)
        @ctx.g.moveTo(x, 0 - @DEFAULT.GRID_TICK_MAJ)
        @ctx.g.lineTo(x, @bbox.height)
        @ctx.g.closePath()
        @ctx.g.stroke()

      @restore(@ctx.g)

    drawYGrid: ->
      @setOrigin(@ctx.g, @bbox.bl.x, @bbox.bl.y)
      row = @configs.gridRows
      interval = @bbox.height / row
      @ctx.g.lineWidth = 1
      @ctx.g.strokeStyle = '#AAA'

      for n in [1..row]
        @ctx.g.beginPath()
        y = Math.floor(n * interval)
        @ctx.g.moveTo(0 - @DEFAULT.GRID_TICK_MAJ, y)
        @ctx.g.lineTo(@bbox.width, y)
        @ctx.g.closePath()
        @ctx.g.stroke()

      @restore(@ctx.g)

    ### Label Axes ###
    labelAxes: ->
      @labelYAxis()
      @labelXAxis()

    labelXAxis: ->
      # See child class

    labelYAxis: ->
      @setOrigin(@ctx.y, 0, @bbox.bl.y)
      row = @configs.gridRows
      interval = @bbox.height / row

      @ctx.y.font = 'italic 8pt sans-serif'
      @ctx.y.scale(1, -1)
      @ctx.y.fillStyle = '#888'
      @ctx.y.textAlign = "right"

      for n in [0..row]
        x = @AXIS_MARGIN_Y - @LABEL_MARGIN
        y = n * interval
        label = Math.roundTo(n * @yrange / row + @ymin, 3).toString()
        @ctx.y.fillText(label, x, -y)

      @restore(@ctx.y)

    restore: (ctx) ->
      ctx.restore()

    onselect: (o, w, h) ->
      @selected = []

    clear: ->
      # Clear the canvas
      for key, ctx of @ctx
        ctx.clearRect(0, 0, @cwidth, @cheight)

    end: ->
      @clear()
      if @configs.qcconfigs
        qcc.end() for qcc in @configs.qcconfigs

    ### Initialize Configuration Modules ###
    manageQCConfigs: ->
      @configs.qcconfigs = []
      for qcc in @configs.qccmodules when qcc not in @configs.qccblacklist
        Module = eval("qcconfigs.#{qcc}")
        module = new Module()
        @configs.qcconfigs.push module

    seriesColor: (idx) ->
      if idx instanceof Array
        needsLow = false
        needsHigh = false
        for i in idx
          needsHigh = true if @selected.indexOf(i)  > -1
          needsLow  = true if @selected.indexOf(i) is -1

        if needsHigh and needsLow then return @DEFAULT.SERIES_CONTAINSHIGHLIGHT
        else if needsHigh         then return @DEFAULT.SERIES_HIGHLIGHT
        else                      return @DEFAULT.SERIES_COLOR

      # If it's not an array, it should be a number
      if @selected.indexOf(idx) > -1
        @DEFAULT.SERIES_HIGHLIGHT
      else @DEFAULT.SERIES_COLOR

    Math.roundTo = (number, precision) ->
      precision = Math.abs(parseInt(precision)) || 0
      multiplier = Math.pow(10, precision)
      Math.round(number * multiplier) / multiplier
