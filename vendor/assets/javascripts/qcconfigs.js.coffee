##
# qcconfigs.js.coffee
#
# The class defines as much of the vis configuration options as possible.
$ ->
  $('.qcconfigs-header').click (e) ->
    $(@).siblings('.qcconfigs-body').slideToggle()

  window.qcconfigs ?= {}

  class qcconfigs.QCConfigs
    constructor: ->
      @init()

    init: (id = null) ->
      if id? then $(id).show()

    end: (id = null) ->
      if id? then $(id).hide()

  class qcconfigs.QCAxisControls extends qcconfigs.QCConfigs
    contructor: ->
      super()

    init: (id = null) ->
      super('#axis-controls')

    end: (id = null) ->
      super('#axis-controls')

  class qcconfigs.QCCarousel extends qcconfigs.QCConfigs
    contructor: ->
      super()

    init: (id = null) ->
      super('#qccarousel-bar')
      super('#carousel-controls')

    end: (id = null) ->
      super('#qccarousel-bar')
      super('#carousel-controls')

  ### Switch Axis Labels ###
  # Override $('input[name=qc-xaxis]:radio').change (e) ->
  qcconfigs.setXAxisControls = (vis, value) ->
      data = vis.getData()
      configs = vis.getConfigs()
      configs.independent = value
      vis.drawChart(data, configs)

  # Override $('input[name=qc-yaxis]:radio').change (e) ->
  qcconfigs.setYAxisControls = (vis, values) ->
    data = vis.getData()
    configs = vis.getConfigs()
    configs.dependent = values
    vis.drawChart(data, configs)
