##
# qchist.js.coffee
#
# This class defines Histogram specific code.
# Overrides quelchart.js.coffee defaults.
$ ->
  class window.QCHistogram extends QuelChart
    constructor: (id = 0) ->
      super(id)
      @bars = []

    drawChart: (data, configs, resetId = false) ->
      super(data, configs, resetId)
      @drawBars()

    ensureDefaults: ->
      @configs.hist ?= {}
      @configs.qccblacklist = []
      @configs.hist.numbins ?= 10

      super()

    setDataBoundaries: ->
      super()

      @configs.hist.binsize = @xrange / @configs.hist.numbins

    drawXGrid: ->
      @setOrigin(@ctx.g, @bbox.bl.x, @bbox.bl.y)
      col = @configs.hist.numbins
      interval = @bbox.width / col
      @ctx.g.strokeStyle = '#AAA'
      @ctx.g.fillStyle = '#AAA'
      @ctx.g.lineWidth = 1

      for n in [1..col]
        @ctx.g.beginPath()
        x = Math.floor(n * interval)
        @ctx.g.moveTo(x, 0)
        @ctx.g.lineTo(x, @DEFAULT.GRID_TICK_MAJ)
        @ctx.g.closePath()
        @ctx.g.stroke()

      @restore(@ctx.g)

    makeBins: ->
      bins = []
      thresh = []
      for i in [0..@configs.hist.numbins - 1]
        bins[i] = []
        thresh[i] = @xmin + (i + 1) / @configs.hist.numbins * @xrange

      x = @configs.independent
      for row, i in data
        j = 0
        j++ while row[x] > thresh[j]
        bins[j].push i

      return bins

    ### Draw Bars ###
    drawBars: ->
      x = @configs.independent
      barwidth = @bbox.width / @configs.hist.numbins
      @setOrigin(@ctx.m, @bbox.bl.x, @bbox.bl.y)
      @bars = []

      bins = @makeBins()

      for row, idx in bins
        xval = row[x]

        xpos = idx / @configs.hist.numbins * @bbox.width
        ypos = row.length / @xcount * @bbox.height
        barheight = Math.abs(ypos)

        @ctx.m.beginPath()
        color = @seriesColor(row)
        @ctx.m.fillStyle = color
        @ctx.m.rect(xpos, 0, barwidth, barheight)
        for id in row
          @bars.push {
            id: id
            left: xpos,
            right: xpos + barwidth,
            top: barheight,
            bottom: 0
          }
        @ctx.m.closePath()
        @ctx.m.fill()

      @restore(@ctx.m)

    ### Label Axes ###
    labelXAxis: ->
      @setOrigin(@ctx.x, @bbox.bl.x, @cheight)
      col = @configs.hist.numbins
      interval = @bbox.width / col
      xidx = @configs.independent

      @ctx.x.font = 'italic 8pt sans-serif'
      @ctx.x.scale(1, -1)
      @ctx.x.fillStyle = '#888'
      @ctx.x.textAlign = 'center'

      for i in [0..@configs.hist.numbins]
        x = i / @configs.hist.numbins * @bbox.width
        y = -@AXIS_MARGIN_X + @LABEL_MARGIN
        label = Math.roundTo(i / @configs.hist.numbins * @xrange + @xmin,
          3).toString()
        @ctx.x.fillText(label, x, y)

      @restore(@ctx.x)

    onselect: (o, w, h) ->
      super()

      canvas = $('.qccanvas-wrapper')[@id].getBoundingClientRect()
      o.x = o.x - (canvas.x + @bbox.bl.x)
      o.y = canvas.top + @bbox.bl.y - o.y

      sb = {
        left: o.x,
        right: o.x + w,
        top: o.y,
        bottom: o.y - h
      }

      bars = @bars.filter (b) ->
        !(b.right < sb.left or b.left > sb.right or
        b.bottom > sb.top or b.top < sb.bottom)

      for bar in bars
        @selected.push bar.id

      @selected

    labelYAxis: ->
      @setOrigin(@ctx.y, 0, @bbox.bl.y)
      row = @configs.gridRows
      interval = @bbox.height / row

      @ctx.y.font = 'italic 8pt sans-serif'
      @ctx.y.scale(1, -1)
      @ctx.y.fillStyle = '#888'
      @ctx.y.textAlign = "right"

      for n in [0..row]
        x = @AXIS_MARGIN_Y - @LABEL_MARGIN
        y = n * interval
        label = Math.roundTo(n * @ycount / row, 3).toString()
        @ctx.y.fillText(label, x, -y)

      @restore(@ctx.y)

    end: ->
      super()
