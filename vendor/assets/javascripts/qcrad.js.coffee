##
# qcrad.js.coffee
#
# This class defines RadVis specific code.
# Overrides quelchart.js.coffee defaults.
$ ->
  class window.QCRad extends QuelChart
    constructor: (id = 0) ->
      super(id)
      @points = []

    drawChart: (data, configs, resetId = false) ->
      super(data, configs, resetId)
      @drawCircle()
      @drawSpokes()
      @labelSpokes()
      @drawPoints()

    ensureDefaults: ->
      @configs.rad ?= {}
      @configs.rad.radius = Math.floor(@bbox.height / 2 - @LABEL_MARGIN)
      @configs.rad.point ?= {}
      @configs.rad.selectedY ?= @configs.numfields
      @configs.qccontrols ?= []
      @configs.qccblacklist = ['QCAxisControls']

      super()

    ### Draw Axes ###
    drawAxes: ->
      # Do nothing

    drawCircle: ->
      @setOrigin(@ctx.x, @bbox.bl.x, @bbox.bl.y)
      @ctx.x.beginPath()
      @ctx.x.lineWidth = 5
      @ctx.x.strokeStyle = '#888'
      @ctx.x.arc(@bbox.cp.x, @bbox.cp.y, @configs.rad.radius, 0, 2 * Math.PI)
      @ctx.x.closePath()
      @ctx.x.stroke()

      @restore(@ctx.x)

    ### Draw Grid ###
    drawXGrid: ->
      # Do nothing

    drawYGrid: ->
      # Do nothing

    drawSpokes: ->
      spokes = @configs.rad.selectedY.length
      angle = 2 * Math.PI / spokes

      @setOrigin(@ctx.g, @bbox.bl.x, @bbox.bl.y)
      @ctx.g.translate(@bbox.cp.x, @bbox.cp.y)
      @ctx.g.lineWidth = 1
      @ctx.g.strokeStyle = '#AAA'

      for i in [0..spokes - 1]
        @ctx.g.beginPath()
        @ctx.g.moveTo(0, 0)
        @ctx.g.lineTo(@configs.rad.radius + @DEFAULT.GRID_TICK_MAJ, 0)
        @ctx.g.closePath()
        @ctx.g.stroke()
        @ctx.g.rotate(angle)

      @restore(@ctx.g)

    ### Sets the min, max, and range values ###
    setDataBoundaries: ->
      @max = {}
      @min = {}
      @range = {}

      for idx in @configs.numfields
        d = for row in @data
          row[idx]

        @max[idx] = d.reduce (a, b) -> Math.max(a, b)
        @min[idx] = d.reduce (a, b) -> Math.min(a, b)
        @range[idx] = @max[idx] - @min[idx]

    ### Draw Points ###
    drawPoints: ->
      selected = @configs.rad.selectedY
      axes = selected.length
      angle = 2 * Math.PI / axes
      radius = @configs.rad.radius

      @setOrigin(@ctx.m, @bbox.bl.x, @bbox.bl.y)
      @ctx.m.translate(@bbox.cp.x, @bbox.cp.y)

      xnorm = 0
      ynorm = 0
      for field, idx in selected
        xseg = radius * Math.cos(angle * idx)
        if xseg > 0 then xnorm += xseg

        yseg = radius * Math.sin(angle * idx)
        if yseg > 0 then ynorm += yseg

      for row, idx in @data
        xpos = 0
        ypos = 0

        for field, i in selected
          val = row[field]
          if isNaN(val) then continue

          mag = (val - @min[field]) * radius / @range[field]
          if isNaN(mag) then continue

          xpos += mag * Math.cos(angle * i)
          ypos += mag * Math.sin(angle * i)

        x = xpos / Math.max(xnorm, radius) * radius
        y = ypos / Math.max(ynorm, radius) * radius

        color = @seriesColor(idx)
        @GLYPHS.circle(x, y, @ctx.m, color)
        @points.push { id: idx, x: x, y: y }

      @restore(@ctx.m)

    ### Label Axes ###
    labelAxes: ->
      # Do Nothing

    labelSpokes: ->
      selected = @configs.rad.selectedY
      headers = @configs.headers
      spokes = selected.length
      angle = 2 * Math.PI / spokes

      @setOrigin(@ctx.x, @bbox.bl.x, @bbox.bl.y)
      @ctx.x.translate(@bbox.cp.x, @bbox.cp.y)
      @ctx.x.font = 'italic 8pt sans-serif'
      @ctx.x.fillStyle = '#888'
      @ctx.x.textAlign = "center"
      @ctx.x.scale(1, -1)
      @ctx.x.rotate(Math.PI / 2)

      for i in [0..spokes - 1]
        y = @configs.rad.radius + @LABEL_MARGIN
        @ctx.x.beginPath()
        @ctx.x.fillText(headers[selected[i]], 0, -y)
        @ctx.x.closePath()
        @ctx.x.rotate(-angle)

      @restore(@ctx.x)

    onselect: (o, w, h) ->
      super()

      canvas = $('.qccanvas-wrapper')[@id].getBoundingClientRect()
      o.x = (o.x - (canvas.x + @bbox.bl.x)) - @bbox.cp.x
      o.y = (canvas.top + @bbox.bl.y - o.y) - @bbox.cp.y

      points = @points.filter (p) ->
        (p.x > o.x) and (p.x < o.x + w) and (p.y < o.y) and (p.y > o.y - h)

      for point in points
        @selected.push point.id

      @selected

    end: ->
      super()

    ### Initialize Controls ###
    manageQCConfigs: ->
      super()
