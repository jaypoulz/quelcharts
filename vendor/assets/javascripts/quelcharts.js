// This is a manifest file that'll specifies the order in which these files will
// be compiled into application.js, which will include all the files listed
// below. This is the bare minimum load order necessary pipeline to work.
//
//  Base class
//= require ./quelchart
//
//  Vis configurations
//= require ./qcconfigs
//= require ./dragSelection
//
//  Child classes
//= require ./qcscatter
//= require ./qcbar
//= require ./qcrad
//= require ./qchist
//= require ./qctree
//
//  Carousel
//= require ./qccarousel
