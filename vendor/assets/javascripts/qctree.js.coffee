##
# qctree.js.coffee
#
# This class defines TreeMap specific code.
# Overrides quelchart.js.coffee defaults.
$ ->
  class window.QCTree extends QuelChart
    constructor: (id = 0) ->
      super(id)
      @leaves = []

    drawChart: (data, configs, resetId = false) ->
      super(data, configs, resetId)
      @drawTree()

    ensureDefaults: ->
      @configs.tree ?= {}
      @configs.qccblacklist = []
      @configs.tree.numleaves ?= 2

      if @tree then delete @tree

      super()

    setDataBoundaries: ->
      super()

    drawXGrid: ->
      # Do nothing

    drawYGrid: ->
      # Do nothing

    drawXAxis: ->
      # Do nothing

    drawYAxis: ->
      # Do nothing

    makeTree: ->
      sortFunc = (a, b) ->
        x = @configs.independent
        if a[x] < b[x] then return -1
        else return 1

      # Tree is already made
      if @tree then return

      @tree = @data.slice(0)
      row.push id for row, id in @tree
      @tree.sort(sortFunc).reverse()

    ### Draw the Actual Tree ###
    drawTree: ->
      @leaves = []

      @makeTree()
      len = @tree.length
      k = @configs.tree.numleaves
      h = Math.ceil((Math.log(k - 1) / Math.log(k)) +
        (Math.log(len) / Math.log(k) - 1))

      @setOrigin(@ctx.m, @bbox.tl.x, @bbox.tl.y)
      levelh = @bbox.height / (h + 1)
      radius = levelh / 2

      @setOrigin(@ctx.t, @bbox.tl.x, @bbox.tl.y)
      @ctx.t.font = 'bold 8pt sans-serif'
      @ctx.t.fillStyle = '#FFF'
      @ctx.t.textAlign = 'center'
      @ctx.t.scale(1, -1)

      ti = 0
      numtodraw = 1
      for l in [0..h]
        y = -levelh * (l + 1) + radius
        for n in [0..numtodraw - 1]
          if ti < len
            id = @tree[ti][@tree[ti].length - 1]
            x = @bbox.cp.x + ((-(numtodraw - numtodraw / 2) + n + .5) * levelh)
            @ctx.m.beginPath()
            @ctx.m.arc(x, y, radius, 0, 2 * Math.PI)
            @ctx.m.fillStyle = @seriesColor(id)
            @ctx.m.closePath()
            @ctx.m.fill()

            @leaves.push
              x: x
              y: y
              radius: radius
              id: id

            # Label the bubble
            label = @tree[ti][@configs.independent]
            @ctx.t.fillText(label, x, -y)

            ti++

        numtodraw *= @configs.tree.numleaves

      @restore(@ctx.m)
      @restore(@ctx.x)

    onselect: (o, w, h) ->
      super()
      canvas = $('.qccanvas-wrapper')[@id].getBoundingClientRect()
      o.x = o.x - (canvas.x + @bbox.bl.x)
      o.y = canvas.top + @bbox.tl.y - o.y

      sb = {
        left: o.x,
        right: o.x + w,
        top: o.y,
        bottom: o.y - h
      }

      leaves = @leaves.filter (l) ->
        !((l.x + l.radius) < sb.left or (l.x - l.radius) > sb.right or
        (l.y - l.radius) > sb.top or (l.y + l.radius) < sb.bottom)

      for leaf in leaves
        @selected.push leaf.id

      @selected

    labelXAxis: ->
      # Do nothing

    labelYAxis: ->
      # Do nothing

    end: ->
      super()
