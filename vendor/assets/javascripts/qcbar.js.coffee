##
# qcbar.js.coffee
#
# This class defines Bar specific code.
# Overrides quelchart.js.coffee defaults.
$ ->
  class window.QCBar extends QuelChart
    constructor: (id = 0) ->
      super(id)
      @bars = []

    drawChart: (data, configs, resetId = false) ->
      super(data, configs, resetId)
      @drawBars()

    ensureDefaults: ->
      @configs.bar ?= {}
      @configs.qccblacklist = []

      super()

    setDataBoundaries: ->
      super()

      @configs.bar.barwidth = @bbox.width * .70 / @xcount
      @barwidth = @configs.bar.barwidth
      @spacewidth = (@bbox.width - (@xcount * @barwidth)) / (@xcount + 1)

    drawXGrid: ->
      # Do nothing

    ### Draw Bars ###
    drawBars: ->
      x = @configs.independent
      y = @configs.dependent[0]

      @setOrigin(@ctx.m, @bbox.bl.x, @bbox.bl.y)
      @bars = []

      for row, idx in @data
        xval = row[x]
        yval = row[y]

        xpos = idx * (@barwidth + @spacewidth) + @spacewidth
        ypos = (yval - @ymin) * @bbox.height / @yrange
        barheight = Math.abs(ypos)

        # Case when bar height is zero
        if barheight is 0
          barheight = 1

        @ctx.m.beginPath()
        color = @seriesColor(idx)
        @ctx.m.fillStyle = color
        @ctx.m.rect(xpos, 0, @barwidth, barheight)
        @bars.push {
          id: idx
          left: xpos,
          right: xpos + @barwidth,
          top: barheight,
          bottom: 0
        }
        @ctx.m.closePath()
        @ctx.m.fill()

      @restore(@ctx.m)

    ### Label Axes ###
    labelXAxis: ->
      @setOrigin(@ctx.x, @bbox.bl.x, @cheight)
      col = @configs.gridColumns
      interval = @bbox.width / col
      xidx = @configs.independent

      @ctx.x.font = 'italic 8pt sans-serif'
      @ctx.x.scale(1, -1)
      @ctx.x.fillStyle = '#888'
      @ctx.x.textAlign = 'center'

      for row, i in @data
        x = i * (@barwidth + @spacewidth) + @spacewidth + @barwidth / 2
        y = -@AXIS_MARGIN_X + @LABEL_MARGIN
        label =
          if xidx in @configs.textfields then row[xidx]
          else Math.roundTo(row[xidx], 3).toString()
        @ctx.x.fillText(label, x, y)

      @restore(@ctx.x)

    onselect: (o, w, h) ->
      super()

      canvas = $('.qccanvas-wrapper')[@id].getBoundingClientRect()
      o.x = o.x - (canvas.x + @bbox.bl.x)
      o.y = canvas.top + @bbox.bl.y - o.y

      sb = {
        left: o.x,
        right: o.x + w,
        top: o.y,
        bottom: o.y - h
      }

      bars = @bars.filter (b) ->
        !(b.right < sb.left or b.left > sb.right or
        b.bottom > sb.top or b.top < sb.bottom)

      for bar in bars
        @selected.push bar.id

      @selected

    end: ->
      super()
