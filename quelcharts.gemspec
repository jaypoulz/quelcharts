$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "quelcharts/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "quelcharts"
  s.version     = Quelcharts::VERSION
  s.authors     = ["Jeremy Poulin"]
  s.email       = ["jaypoulz@gmail.com"]
  s.homepage    = "http://www.cs.uml.edu/~jpoulin"
  s.summary     = "Quelcharts is an visualization library built on WebGl."
  s.description = "Quelcharts is a library for creating mobile friendly visualizations on the web. It is built directly upon webGL."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.6"

  s.add_development_dependency "sqlite3"
end
