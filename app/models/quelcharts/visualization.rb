class Visualization < ActiveRecord::Base
  serialize :data, JSON

  def to_hash()
    h = {
      id: id,
      data: data,
      configs: configs
    }
  end
end
